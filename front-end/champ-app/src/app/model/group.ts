import { Team } from './team';

export class Group {
    id?: number;
    name: string;
    round: number;
    champId?: number;
    teams?: Array<Team>; 
  }
  