export class Result {
    id?: number;
    winTeam?: number;
    team1?: number;
    team2?: number;
    groupId?: number;
    score1?: number;
    score2?: number;
  }
  