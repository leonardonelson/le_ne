import { TeamStats } from "./team-stats";
import { Result } from "./result";

export class GroupSummary {
  groupName?: string;
  groupId?: number;
  teamsStats?: Array<TeamStats>;
  results?: Array<Result>;    
}
  