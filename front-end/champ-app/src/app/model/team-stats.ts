export class TeamStats {
    teamName?: string;
    teamId?: number;
    wins?: number;
    points?: number;
    balance_round?: number;
  }
  