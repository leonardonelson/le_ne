import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpClientModule }    from '@angular/common/http';

import { AppRoutingModule }     from './app-routing.module';

import { AppComponent }         from './app.component';
import { ChampionshipComponent }   from './components/championship/championship.component';
import { SecondPhaseComponent }  from './components/secondphase/secondphase.component';
import { FirstPhaseComponent }      from './components/firstphase/firstphase.component';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpClientModule,
    Ng4LoadingSpinnerModule.forRoot()
  ],
  declarations: [
    AppComponent,
    ChampionshipComponent,
    FirstPhaseComponent,
    SecondPhaseComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
