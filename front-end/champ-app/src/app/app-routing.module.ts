import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ChampionshipComponent } from './components/championship/championship.component';
import { FirstPhaseComponent } from './components/firstphase/firstphase.component';
import { SecondPhaseComponent } from './components/secondphase/secondphase.component';

const routes: Routes = [
  { path: '', redirectTo: '/championship', pathMatch: 'full' },
  { path: 'championship', component: ChampionshipComponent },
  { path: 'firstphase/:champId', component: FirstPhaseComponent },
  { path: 'secondphase/:champId', component: SecondPhaseComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
