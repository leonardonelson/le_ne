import { Group } from '../model/group';

export const GROUPS_QUARTER_FINALS: Group[] = [
  { name: 'Grupo A', round: 4},
  { name: 'Grupo B', round: 4},
  { name: 'Grupo C', round: 4},
  { name: 'Grupo D', round: 4}
];
