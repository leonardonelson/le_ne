import { Group } from '../model/group';

export const GROUPS_SEMIFINALS: Group[] = [
  { name: 'Grupo A', round: 5},
  { name: 'Grupo B', round: 5}
];
