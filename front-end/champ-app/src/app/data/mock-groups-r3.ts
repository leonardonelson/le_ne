import { Group } from '../model/group';

export const GROUPS_ROUND_3: Group[] = [
  { name: 'Grupo A', round: 3},
  { name: 'Grupo B', round: 3},
  { name: 'Grupo C', round: 3},
  { name: 'Grupo D', round: 3},
  { name: 'Grupo E', round: 3},
  { name: 'Grupo F', round: 3},
  { name: 'Grupo G', round: 3},
  { name: 'Grupo H', round: 3}
];
