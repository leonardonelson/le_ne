import { Group } from '../model/group';

export const GROUPS: Group[] = [
  { name: 'Grupo A', round: 1},
  { name: 'Grupo B', round: 1},
  { name: 'Grupo C', round: 1},
  { name: 'Grupo D', round: 1},
  { name: 'Grupo E', round: 1},
  { name: 'Grupo F', round: 1},
  { name: 'Grupo G', round: 1},
  { name: 'Grupo H', round: 1},
  { name: 'Grupo I', round: 1},
  { name: 'Grupo J', round: 1},
  { name: 'Grupo K', round: 1},
  { name: 'Grupo L', round: 1},
  { name: 'Grupo M', round: 1},
  { name: 'Grupo N', round: 1},
  { name: 'Grupo O', round: 1},
  { name: 'Grupo P', round: 1}
];
