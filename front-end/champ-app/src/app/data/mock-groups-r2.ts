import { Group } from '../model/group';

export const GROUPS_ROUND_2: Group[] = [
  { name: 'Grupo A', round: 2},
  { name: 'Grupo B', round: 2},
  { name: 'Grupo C', round: 2},
  { name: 'Grupo D', round: 2},
  { name: 'Grupo E', round: 2},
  { name: 'Grupo F', round: 2},
  { name: 'Grupo G', round: 2},
  { name: 'Grupo H', round: 2},
  { name: 'Grupo I', round: 2},
  { name: 'Grupo J', round: 2},
  { name: 'Grupo K', round: 2},
  { name: 'Grupo L', round: 2},
  { name: 'Grupo M', round: 2},
  { name: 'Grupo N', round: 2},
  { name: 'Grupo O', round: 2},
  { name: 'Grupo P', round: 2}
];
