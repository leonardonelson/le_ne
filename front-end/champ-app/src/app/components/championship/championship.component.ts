import { Component, OnInit } from '@angular/core';
import { Championship } from '../../model/championship';
import { ChampService } from '../../service/champ.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-championship',
  templateUrl: './championship.component.html'
})
export class ChampionshipComponent {

  constructor(private champService: ChampService,
              private router: Router) { }


  createChamp(champName: string): void {
    const champ = new Championship();
    champ.name = champName;
    this.champService.addChampionship(champ)
      .subscribe(championship => this.router.navigateByUrl('/firstphase/' + championship.id));
  }
}
