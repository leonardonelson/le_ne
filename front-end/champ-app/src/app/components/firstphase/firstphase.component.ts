import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Router } from '@angular/router';
import { Team } from '../../model/team';
import { TEAMS } from '../../data/mock-teams';
import { GROUPS } from '../../data/mock-groups';
import { Group } from '../../model/group';
import { GroupSummary } from '../../model/group-summary';
import { UtilsService } from 'src/app/service/utils-service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-firstphase',
  templateUrl: './firstphase.component.html'
})
export class FirstPhaseComponent implements OnInit {
  NR_OF_TEAMS_PER_GROUP: number = 5;
  FIRST_ROUND = 1;
  teams: Array<Team> = new Array<Team>();
  groups: Array<Group> = new Array<Group>();
  groupsSummary: Array<GroupSummary> = new Array<GroupSummary>();

  constructor(private route: ActivatedRoute,
    private utilService: UtilsService,
    private router: Router,
    private spinnerService: Ng4LoadingSpinnerService) { }

  ngOnInit() {
    this.spinnerService.show();
    this.startFirstPhase().then(_ => {
      this.spinnerService.hide();
    });
  }

  async startFirstPhase(): Promise<void> {
    const champId = +this.route.snapshot.paramMap.get('champId');

    this.teams = await this.utilService.createTeams(TEAMS);
    this.groups = await this.utilService.createGroups(GROUPS, champId, this.FIRST_ROUND);
    this.groups = await this.utilService.assignTeamsToGroups(this.groups, this.teams, this.NR_OF_TEAMS_PER_GROUP);
    await this.utilService.createResults(this.groups);

    this.groupsSummary = await this.utilService.fetchResults(this.groups);
  }

  skipToSecondPhase(): void {
    const champId = +this.route.snapshot.paramMap.get('champId');
    this.router.navigateByUrl('/secondphase/' + champId);
  }

  findTeam(id: Number): Team {
    return this.teams.find(team => team.id === id);
  }

}
