import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GROUPS_ROUND_2 } from '../../data/mock-groups-r2';
import { GROUPS_ROUND_3 } from '../../data/mock-groups-r3';
import { GROUPS_QUARTER_FINALS } from '../../data/mock-groups-r4';
import { GROUPS_SEMIFINALS } from '../../data/mock-groups-r5';
import { GROUP_FINAL } from '../../data/mock-groups-r6';
import { GroupSummary } from '../../model/group-summary';
import { ChampService } from '../../service/champ.service';
import { Group } from '../../model/group';
import { Observable, forkJoin } from 'rxjs';
import { Result } from '../../model/result';
import { Team } from '../../model/team';
import { UtilsService } from 'src/app/service/utils-service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

@Component({
  selector: 'app-second-phase',
  templateUrl: './secondphase.component.html'
})
export class SecondPhaseComponent implements OnInit {

  private CHAMP_ID: number;

  private FIRST_ROUND = 1;
  private ROUND_OF_16 = 2;
  private ROUND_OF_8 = 3;
  private QUARTER_FINALS = 4;
  private SEMI_FINALS = 5;
  private FINAL = 6;
  
  private TEAMS_PER_GROUP = 2;

  roundOf16GroupsSummary = new Array<GroupSummary>();
  roundOf8GroupsSummary = new Array<GroupSummary>();
  quarterFinalsGroupsSummary = new Array<GroupSummary>();
  semiFinalsGroupsSummary = new Array<GroupSummary>();
  finalGroupsSummary = new Array<GroupSummary>();

  constructor(
    private route: ActivatedRoute,
    private champService: ChampService,
    private utilService: UtilsService,
    private spinnerService: Ng4LoadingSpinnerService
  ) {}

  ngOnInit(): void {
    this.spinnerService.show();
    this.CHAMP_ID = +this.route.snapshot.paramMap.get('champId');
    this.runPlayOffs().then(_ => this.spinnerService.hide());
  }

  async runPlayOffs(): Promise<void> {
    this.roundOf16GroupsSummary = this.roundOf16GroupsSummary.concat (await this.runRoundOf16());
    this.roundOf8GroupsSummary = this.roundOf8GroupsSummary.concat (await this.runRoundOf8());
    this.quarterFinalsGroupsSummary = this.quarterFinalsGroupsSummary.concat(await this.runQuarterFinals());
    this.semiFinalsGroupsSummary = this.semiFinalsGroupsSummary.concat(await this.runSemiFinals());
    this.finalGroupsSummary = this.finalGroupsSummary.concat(await this.runFinal());
  }

  async runRoundOf16(): Promise<Array<GroupSummary>> {
    const groupPhaseResults: Array<GroupSummary> = await this.champService.findResultsByChamp(
      this.CHAMP_ID, this.FIRST_ROUND).toPromise();
      const classifiedTeams = new Array<Team>();
      groupPhaseResults.forEach(groupSummary => {
        const firstRankedTeam: Team = {"id": groupSummary.teamsStats[0].teamId, "name": groupSummary.teamsStats[0].teamName}
        const secondRankedTeam: Team = {"id": groupSummary.teamsStats[1].teamId, "name": groupSummary.teamsStats[1].teamName}
        classifiedTeams.push(firstRankedTeam, secondRankedTeam);
      });

      return await this.runRound(classifiedTeams, GROUPS_ROUND_2, this.ROUND_OF_16);
  }

  async runRoundOf8(): Promise<Array<GroupSummary>> {
    return await this.runRound(this.getClassifiedTeams(this.roundOf16GroupsSummary), GROUPS_ROUND_3, this.ROUND_OF_8);
  }

  async runQuarterFinals(): Promise<Array<GroupSummary>> {
    return await this.runRound(this.getClassifiedTeams(this.roundOf8GroupsSummary), GROUPS_QUARTER_FINALS, this.QUARTER_FINALS);
  }

  async runSemiFinals(): Promise<Array<GroupSummary>> {
    return await this.runRound(this.getClassifiedTeams(this.quarterFinalsGroupsSummary), GROUPS_SEMIFINALS, this.SEMI_FINALS);
  }

  async runFinal(): Promise<Array<GroupSummary>> {
    return await this.runRound(this.getClassifiedTeams(this.semiFinalsGroupsSummary), GROUP_FINAL, this.FINAL);
  }

  getClassifiedTeams(results: Array<GroupSummary>): Array<Team> {
    const classifiedTeams = new Array<Team>();
    results.forEach(groupSummary => {
      classifiedTeams.push({"name": groupSummary.teamsStats[0].teamName, "id": groupSummary.teamsStats[0].teamId});
    });
    return classifiedTeams;
  }

  async runRound(teams: Array<Team>, groups: Array<Group>, round: number): Promise<Array<GroupSummary>> {
    groups = await this.utilService.createGroups(groups, this.CHAMP_ID, round);  
    groups = await this.utilService.assignTeamsToGroups(groups, teams, this.TEAMS_PER_GROUP);
    await this.utilService.createResults(groups);

    return await await this.champService.findResultsByChamp(
      this.CHAMP_ID, round).toPromise();
  }

}
