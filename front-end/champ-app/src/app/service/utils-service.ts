import { Result } from "../model/result";
import { Group } from "../model/group";
import { Injectable } from "@angular/core";
import { ChampService } from "./champ.service";
import { Team } from "../model/team";
import { GroupSummary } from "../model/group-summary";

@Injectable({
    providedIn: 'root'
  })
export class UtilsService {

    constructor(private champService: ChampService) { }

    async createTeams(teams: Array<Team>): Promise<Array<Team>> {
      const createTeamsPromises = new Array<Promise<Team>>();
      teams.forEach(team => createTeamsPromises.push(this.champService.addTeam(team).toPromise()));
      return await Promise.all(createTeamsPromises);
    }

    generateScore(result: Result): Result {
        result.score1 = 16;
        result.score2 = Math.floor(Math.random() * 16) + 1;
        while(result.score2 == result.score1) {
          result.score2 = Math.floor(Math.random() * 16) + 1;
        }
        let teams = [result.team1, result.team2];
        result.winTeam = teams[Math.floor(Math.random()*teams.length)];
        return result;
    }

    async createGroups(groups: Array<Group>, champId: number, round: number): Promise<Array<Group>> {
        const createGroupPromises = new Array<Promise<Group>>();
        groups.forEach(group => {
          group.champId = champId;
          group.round = round;
          createGroupPromises.push(this.champService.addGroup(group).toPromise());
        });
        return await Promise.all(createGroupPromises);
      }
    
    async assignTeamsToGroups(groups: Array<Group>, teams: Array<Team>, teamsPerGroup: number): Promise<Array<Group>> {
        const assignTeamsToGroupsPromises = new Array<Promise<Object>>();
        let teamIndex = 0;
        groups.forEach((group, idx) => {
          groups[idx].teams = new Array<Team>();
          for(let i = teamIndex; i < teamIndex + teamsPerGroup; i++) {
            groups[idx].teams.push(teams[i]);
            assignTeamsToGroupsPromises.push(this.champService.addTeamToGroup(group.id, teams[i].id).toPromise());
          }
          teamIndex+=teamsPerGroup;
        });
        await assignTeamsToGroupsPromises;
        return Promise.resolve(groups);
      }
    
    async createResults(groups: Array<Group>): Promise<Array<Result>> {
        const results = new Array<Result>();
        const createResultsPromises = new Array<Promise<Result>>();
        groups.forEach(group => {
          group.teams.forEach((team, idx) => {
            for(let i = idx + 1; i < group.teams.length; i++) {
              let result: Result = {'team1': team.id,'team2': group.teams[i].id, 'groupId': group.id  }
              result = this.generateScore(result);
              results.push(result);
            }
          });
        });
        results.forEach(result => createResultsPromises.push(this.champService.addResult(result).toPromise()));
        return await Promise.all(createResultsPromises);
      }
    
    async fetchResults(groups: Array<Group>): Promise<Array<GroupSummary>> {
        const findGroupResultsPromises = new Array<Promise<GroupSummary>>();
        groups.forEach(group => {
          findGroupResultsPromises.push(this.champService.findResultsByGroup(group.id).toPromise());
        });
        return Promise.all(findGroupResultsPromises);
      }
}