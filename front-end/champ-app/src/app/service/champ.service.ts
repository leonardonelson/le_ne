import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { Team } from '../model/team';
import { Group } from '../model/group';
import { Championship } from '../model/championship';
import { Result } from '../model/result';
import { GroupSummary } from '../model/group-summary';
import { environment } from '../../environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json'})
};

@Injectable({ providedIn: 'root' })
export class ChampService {

  private champUrl = environment.registrationApiUrl + '/championship';
  private teamUrl = environment.registrationApiUrl + '/team';
  private groupUrl = environment.registrationApiUrl + '/group';
  private resultUrl = environment.ingameApiUrl + '/result';

  constructor(private http: HttpClient) { }


  addChampionship (champ: Championship): Observable<Championship> {
    return this.http.post<Championship>(this.champUrl, champ, httpOptions).pipe(
      tap((newChamp: Championship) => this.log(`added championship w/ id=${newChamp.id}`)),
      catchError(this.handleError<Championship>('addChampionship'))
    );
  }

  addTeam(team: Team): Observable<Team> {
    return this.http.post<Team>(this.teamUrl, team, httpOptions).pipe(
      tap((newTeam: Team) => this.log(`added team w/ id=${newTeam.id}`)),
      catchError(this.handleError<Team>('addTeam'))
    );
  }

  addGroup(group: Group): Observable<Group> {
    return this.http.post<Team>(this.groupUrl, group, httpOptions).pipe(
      tap((newGroup: Group) => this.log(`added group w/ id=${newGroup.id}`)),
      catchError(this.handleError<Group>('addGroup'))
    );
  }

  addTeamToGroup(groupId: Number, teamId: Number): Observable<Object> {
    return this.http.post(this.groupUrl + "/team", {'groupId': groupId, 'teamId': teamId}, httpOptions).pipe(
      tap((response: Object) => this.log(`added team w/ id=${teamId} to group w/ id=${groupId}`)),
      catchError(this.handleError<Object>('addTeamToGroup'))
    );
  }

  addResult(result: Result): Observable<Result> {
    return this.http.post(this.resultUrl, result, httpOptions).pipe(
      tap((newResult: Result) => this.log(`added result w/ id=${newResult.id}`)),
      catchError(this.handleError<Result>('addResult'))
    );
  }

  findResultsByGroup(groupId: number): Observable<GroupSummary>{
    return this.http.get<GroupSummary>(`${this.resultUrl}/group/${groupId}`).pipe(
      tap(_ => this.log(`found group results for group "${groupId}"`)),
      catchError(this.handleError<GroupSummary>('findResultsByGroup'))
    );
  }

  findResultsByChamp(champId: number, round: number): Observable<Array<GroupSummary>> {
    return this.http.post(this.resultUrl + "/championship", {"champId": champId, "round": round}, httpOptions).pipe(
      tap((results: Array<GroupSummary>) => this.log(`found champ results for champ w/ id=${champId}`)),
      catchError(this.handleError<Array<GroupSummary>>('findResultsByChamp'))
    );
  }


  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  /** Log a ChampionshipService message */
  private log(message: string) {
    //console.info(`ChampionshipService: ${message}`);
  }
}
