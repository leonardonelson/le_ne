# API de Simulação de Campeonatos

### Arquitetura
![alt text](img/architecture.jpg)

A arquitetura das APIs está composta da seguinte maneira:

* **Registration service**: este serviço REST é responsável por todas as operações
de manipulação de dados relacionados à campeonatos, times e grupos

* **In-Game**: este segundo serviço REST é responsável pelas operações
relacionadas aos resultados das partidas. Foi decidido separá-lo em uma API
exclusiva por conta do alto número de requisiçoes que o mesmo irá receber
(requisições de front-end e também requisições dos servidores da partida
para registrar os resultados dos jogos). Portanto isso fornecerá uma maior
escalabilidade ao serviço, evitando possíveis gargalos.

* **Database Service**: Ambos os serviços estão conectados à um terceiro
serviço de banco de dados.


O servidor de front end está hospedando uma aplicação em Angular JS, apenas
simulando/demonstrando a utilização dos serviços REST.

### Modelo de dados e documentação

[Documentação API](https://app.swaggerhub.com/apis/leonardopensante/championship/1.0.0)
