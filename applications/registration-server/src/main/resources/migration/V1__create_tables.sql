CREATE TABLE championship
(
  id     BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(255)
);

CREATE TABLE champ_group
(
  id     BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(255),
  champ_id BIGINT(20),
  round INT(2),

  constraint foreign key (champ_id) references championship (id)
);

CREATE TABLE team
(
  id     BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  name VARCHAR(255)
);

CREATE TABLE result
(
  id     BIGINT(20) PRIMARY KEY NOT NULL AUTO_INCREMENT,
  win_team BIGINT(20),
  team1 BIGINT(20),
  team2 BIGINT(20),
  group_id BIGINT(20),
  score1 INT(2),
  score2 INT(2),

  constraint foreign key (win_team) references team (id),
  constraint foreign key (team1) references team (id),
  constraint foreign key (team2) references team (id)
);

CREATE TABLE group_team
(
    group_id BIGINT(20),
    team_id BIGINT(20),

    constraint foreign key (group_id) references champ_group (id),
    constraint foreign key (team_id) references team (id)
);
