package com.esports.championship.emulator;

import com.esports.championship.emulator.championships.JdbcChampionshipRepository;
import com.esports.championship.emulator.groups.JdbcGroupRepository;
import com.esports.championship.emulator.teams.JdbcTeamRepository;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import javax.sql.DataSource;

@SpringBootApplication
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public JdbcChampionshipRepository jdbcChampionshipRepository(DataSource dataSource) {
        return new JdbcChampionshipRepository(dataSource);
    }

    @Bean
    public JdbcTeamRepository jdbcTeamRepository(DataSource dataSource) {
        return new JdbcTeamRepository(dataSource);
    }

    @Bean
    public JdbcGroupRepository jdbcGroupRepository(DataSource dataSource) {
        return new JdbcGroupRepository(dataSource);
    }
}
