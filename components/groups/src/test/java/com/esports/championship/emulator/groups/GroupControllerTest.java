package com.esports.championship.emulator.groups;

import com.esports.championship.emulator.apisupport.AddTeamToGroupRequest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.*;

public class GroupControllerTest {

    GroupRepository groupRepository = mock(GroupRepository.class);
    GroupController groupController = new GroupController(groupRepository);

    @Test
    public void testCreateGroup() {
        // Given
        Group group = new Group();
        group.setChampId(1);
        group.setName("A");
        group.setRound(Round.FIRST);
        Group mockGroup = new Group(1L, 1L, "A", Round.FIRST);
        doReturn(mockGroup).when(groupRepository).save(group);
        // When
        ResponseEntity<Group> response = groupController.createGroup(group);
        // Then
        verify(groupRepository).save(group);
        Group responseGroup = response.getBody();
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(responseGroup);
        Assert.assertNotNull(responseGroup.getId());
        Assert.assertEquals(mockGroup.getId(), responseGroup.getId());
        Assert.assertEquals(mockGroup.getName(), responseGroup.getName());
        Assert.assertEquals(mockGroup.getRound(), responseGroup.getRound());
        Assert.assertEquals(mockGroup.getChampId(), responseGroup.getChampId());
    }

    @Test
    public void testFindGroupById() {
        // Given
        final long groupId = 1L;
        Group mockGroup = new Group(1L, 1L, "A", Round.FIRST);
        doReturn(mockGroup).when(groupRepository).find(groupId);
        // When
        ResponseEntity<Group> response = groupController.findGroup(groupId);
        // Then
        verify(groupRepository).find(groupId);
        Group group = response.getBody();
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(group);
        Assert.assertEquals(groupId, group.getId());
        Assert.assertEquals(mockGroup.getName(), group.getName());
        Assert.assertEquals(mockGroup.getChampId(), group.getChampId());
        Assert.assertEquals(mockGroup.getRound(), group.getRound());
    }

    @Test
    public void testAddTeamToGroup() {
        // Given
        AddTeamToGroupRequest addTeamToGroupRequest = new AddTeamToGroupRequest();
        addTeamToGroupRequest.setGroupId(1);
        addTeamToGroupRequest.setTeamId(1);
        doReturn(true).when(groupRepository).addTeamToGroup(addTeamToGroupRequest);
        // When
        ResponseEntity<HttpStatus> response = groupController.addTeamToGroup(addTeamToGroupRequest);
        // Then
        verify(groupRepository).addTeamToGroup(addTeamToGroupRequest);
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
    }
}
