package com.esports.championship.emulator.groups;

import com.esports.championship.emulator.championships.Championship;
import com.esports.championship.emulator.championships.ChampionshipRepository;
import com.esports.championship.emulator.championships.JdbcChampionshipRepository;
import com.esports.championship.emulator.testsupport.TestDataSourceFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

public class GroupRepositoryTest {
    private DataSource dataSource = TestDataSourceFactory.create("championships");
    private JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
    private GroupRepository groupRepository = new JdbcGroupRepository(dataSource);
    private ChampionshipRepository championshipRepository = new JdbcChampionshipRepository(dataSource);

    @Before
    public void setUp() throws Exception {
        jdbcTemplate.execute("DELETE FROM result");
        jdbcTemplate.execute("DELETE FROM group_team");
        jdbcTemplate.execute("DELETE FROM team");
        jdbcTemplate.execute("DELETE FROM champ_group");
        jdbcTemplate.execute("DELETE FROM championship");
    }

    @Test
    public void testCreateGroup() {
        Championship championship = championshipRepository.save(new Championship(1L, "PRO LEAGUE"));
        Group group = new Group();
        group.setName("A");
        group.setChampId(championship.getId());
        group.setRound(Round.FIRST);

        Group savedGroup = groupRepository.save(group);

        Assert.assertNotNull(savedGroup);
        Assert.assertNotNull(savedGroup.getId());
        Assert.assertEquals(group.getName(), savedGroup.getName());
        Assert.assertEquals(group.getRound(), savedGroup.getRound());
        Assert.assertEquals(group.getChampId(), savedGroup.getChampId());
    }
}
