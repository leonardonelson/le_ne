package com.esports.championship.emulator.groups;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum Round {
    FIRST(1),
    SECOND(2),
    THIRD(3),
    QUARTER_FINAL(4),
    SEMI_FINAL(5),
    FINAL(6);

    private final int round;

    Round(int round) {
        this.round = round;
    }

    @JsonCreator
    public static Round valueOf(int round) {
        Round[] rounds = Round.values();
        for (Round r: rounds) {
            if(r.getRoundValue() == round) {
                return r;
            }
        }
        throw new IllegalArgumentException("Invalid Round: " + round);
    }

    @JsonValue
    public int getRoundValue() {
        return this.round;
    }

}
