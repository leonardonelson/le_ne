package com.esports.championship.emulator.groups;

import com.esports.championship.emulator.apisupport.AddTeamToGroupRequest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

@Repository
public class JdbcGroupRepository implements GroupRepository {

    private final String INSERT_GROUP_SQL = "INSERT INTO champ_group (name, champ_id, round) VALUES (?, ?, ?)";
    private final String FIND_GROUP_SQL = "SELECT * FROM champ_group WHERE id = ?";
    private final String INSERT_TEAM_GROUP_SQL = "INSERT INTO group_team (group_id, team_id) VALUES (?, ?)";
    private final String FIND_GROUPS_BY_CHAMP_AND_ROUND = "select * from champ_group where champ_id = ? and round = ?;";
    private JdbcTemplate jdbcTemplate;
    private final RowMapper<Group> groupRowMapper;
    private final ResultSetExtractor<Group> extractor;

    public JdbcGroupRepository(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);

        groupRowMapper = (rs, rowNum) ->
                new Group(rs.getLong("id"),
                        rs.getLong("champ_id"),
                        rs.getString("name"),
                        Round.valueOf(rs.getInt("round")));

        extractor = (rs) -> rs.next() ? groupRowMapper.mapRow(rs, 1) : null;
    }

    @Override
    public Group save(Group group) {
        GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_GROUP_SQL, RETURN_GENERATED_KEYS);
            ps.setString(1, group.getName());
            ps.setLong(2, group.getChampId());
            ps.setInt(3, group.getRound().getRoundValue());
            return ps;
        }, keyHolder);

        return find(keyHolder.getKey().longValue());
    }

    @Override
    public Group find(long id) {
        return jdbcTemplate.query(
                FIND_GROUP_SQL,
                new Object[]{id},
                extractor
        );
    }

    @Override
    public boolean addTeamToGroup(AddTeamToGroupRequest addTeamToGroupRequest) {
        return jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(INSERT_TEAM_GROUP_SQL);
            ps.setLong(1, addTeamToGroupRequest.getGroupId());
            ps.setLong(2, addTeamToGroupRequest.getTeamId());
            return ps;
        }) > 0;
    }

    @Override
    public List<Group> findAllByChampIdAndRound(Long champId, Integer round) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList(
                FIND_GROUPS_BY_CHAMP_AND_ROUND,
                new Object[]{champId, round}
        );

        List<Group> groups = new ArrayList<>();

        for (Map row : rows) {
            groups.add(new Group((Long) row.get("id"),
                    (Long) row.get("champ_id"),
                    (String) row.get("name"),
                    Round.valueOf((Integer) row.get("round"))));
        }

        return groups;
    }
}
