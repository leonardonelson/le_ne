package com.esports.championship.emulator.groups;

import com.esports.championship.emulator.apisupport.AddTeamToGroupRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class GroupController {

    private static final Logger log = LoggerFactory.getLogger(GroupController.class.getName());

    private GroupRepository groupRepository;

    public GroupController(GroupRepository groupRepository) {
        this.groupRepository = groupRepository;
    }

    @PostMapping("/group")
    public ResponseEntity<Group> createGroup(@RequestBody Group newGroup) {
        log.info("Adding a new Group: {}", newGroup.getName());
        newGroup = groupRepository.save(newGroup);
        return new ResponseEntity<>(newGroup, HttpStatus.OK);
    }

    @GetMapping("/group/{groupId}")
    public ResponseEntity<Group> findGroup(@PathVariable("groupId") long groupId) {
        Group group = groupRepository.find(groupId);
        return new ResponseEntity<>(group, HttpStatus.OK);
    }

    @PostMapping("/group/team")
    public ResponseEntity<HttpStatus> addTeamToGroup(@RequestBody AddTeamToGroupRequest body) {
        log.info("Adding the team {} to the group {}", body.getTeamId(), body.getGroupId());
        boolean teamAdded = groupRepository.addTeamToGroup(body);
        return new ResponseEntity<>(teamAdded ? HttpStatus.OK : HttpStatus.BAD_REQUEST);
    }

}
