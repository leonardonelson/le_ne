package com.esports.championship.emulator.apisupport;

public class AddTeamToGroupRequest {
    private long groupId;
    private long teamId;

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }
}
