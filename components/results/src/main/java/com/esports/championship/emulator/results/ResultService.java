package com.esports.championship.emulator.results;

import java.util.List;

public interface ResultService {
    ResultSummary findGroupResultsSummarized(Long groupId);

    List<ResultSummary> findChampResultsByRound(Long champId, Integer round);

    Result save(Result result);
}
