package com.esports.championship.emulator.results;

import com.esports.championship.emulator.apisupport.GetChampResultsRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ResultController {

    private static Logger log = LoggerFactory.getLogger(ResultController.class.getName());
    private ResultService resultService;

    public ResultController(ResultService resultService) {
        this.resultService = resultService;
    }

    @PostMapping("/result")
    public ResponseEntity<Result> createResult(@RequestBody Result result) {
        log.info("Creating result Team {} x Team {} | Win Team: {} || Score: {} x {}",
                result.getTeam1(), result.getTeam2(), result.getWinTeam(), result.getScore1(),
                result.getScore2());
        result = resultService.save(result);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }

    @GetMapping("/result/group/{groupId}")
    public ResponseEntity<ResultSummary> findGroupResults(@PathVariable("groupId") long groupId) {
        log.info("Finding results for group {}", groupId);
        ResultSummary results = resultService.findGroupResultsSummarized(groupId);
        return new ResponseEntity<>(results, HttpStatus.OK);
    }

    @PostMapping("/result/championship")
    public ResponseEntity<List<ResultSummary>> getResultsByRound(@RequestBody GetChampResultsRequest request) {
        log.info("Finding championship ID {} results per round: {}",request.getChampId(), request.getRound());
        List<ResultSummary> roundGroupSummaries = resultService.findChampResultsByRound(
                request.getChampId(), request.getRound()
        );
        return new ResponseEntity<>(roundGroupSummaries, HttpStatus.OK);
    }
}
