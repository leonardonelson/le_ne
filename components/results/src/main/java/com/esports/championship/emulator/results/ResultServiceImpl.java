package com.esports.championship.emulator.results;

import com.esports.championship.emulator.groups.Group;
import com.esports.championship.emulator.groups.GroupRepository;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ResultServiceImpl implements ResultService {

    private ResultsRepository resultsRepository;
    private GroupRepository groupRepository;

    public ResultServiceImpl(ResultsRepository resultsRepository, GroupRepository groupRepository) {
        this.resultsRepository = resultsRepository;
        this.groupRepository = groupRepository;
    }

    @Override
    public ResultSummary findGroupResultsSummarized(Long groupId) {
        ResultSummary resultSummary = new ResultSummary();
        resultSummary.setGroupId(groupId);
        resultSummary.setGroupName(groupRepository.find(groupId).getName());
        resultSummary.setTeamsStats(resultsRepository.findTeamsStatsByGroup(groupId));
        resultSummary.setResults(resultsRepository.findResultsByGroup(groupId));
        return resultSummary;
    }

    @Override
    public List<ResultSummary> findChampResultsByRound(Long champId, Integer round) {
        List<ResultSummary> roundGroupSummaries = new ArrayList<>();
        List<Group> roundGroups = groupRepository.findAllByChampIdAndRound(champId, round);
        for (Group group : roundGroups) {
            roundGroupSummaries.add(findGroupResultsSummarized(group.getId()));
        }
        return roundGroupSummaries;
    }

    @Override
    public Result save(Result result) {
        return this.resultsRepository.save(result);
    }
}
