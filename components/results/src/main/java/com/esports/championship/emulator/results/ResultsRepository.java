package com.esports.championship.emulator.results;

import com.esports.championship.emulator.teams.TeamStats;
import com.esports.championship.emulator.persistencesupport.BaseRepository;

import java.util.List;

public interface ResultsRepository extends BaseRepository<Result> {
    List<Result>  findResultsByGroup(long groupId);

    List<TeamStats> findTeamsStatsByGroup(long groupId);
}
