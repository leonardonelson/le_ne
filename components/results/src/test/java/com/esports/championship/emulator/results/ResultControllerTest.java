package com.esports.championship.emulator.results;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.*;

public class ResultControllerTest {

    ResultService resultService = mock(ResultService.class);
    ResultController resultController = new ResultController(resultService);

    @Test
    public void testCreateResult() {
        // Given
        Result result = new Result();
        result.setGroupId(1);
        result.setScore1(16);
        result.setScore2(14);
        result.setTeam1(1234);
        result.setTeam2(12345);
        result.setWinTeam(1234);
        doReturn(result).when(resultService).save(result);
        // When
        ResponseEntity<Result> response = resultController.createResult(result);
        // Then
        verify(resultService).save(result);
        Result responseResult = response.getBody();
        Assert.assertNotNull(responseResult);
        Assert.assertNotNull(responseResult.getId());
        Assert.assertEquals(result.getGroupId(), responseResult.getGroupId());
        Assert.assertEquals(result.getScore1(), responseResult.getScore1());
        Assert.assertEquals(result.getScore2(), responseResult.getScore2());
        Assert.assertEquals(result.getTeam1(), responseResult.getTeam1());
        Assert.assertEquals(result.getTeam2(), responseResult.getTeam2());
        Assert.assertEquals(result.getWinTeam(), responseResult.getWinTeam());
    }
}
