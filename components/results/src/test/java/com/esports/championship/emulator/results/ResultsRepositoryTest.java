package com.esports.championship.emulator.results;

import com.esports.championship.emulator.championships.Championship;
import com.esports.championship.emulator.championships.ChampionshipRepository;
import com.esports.championship.emulator.championships.JdbcChampionshipRepository;
import com.esports.championship.emulator.groups.Group;
import com.esports.championship.emulator.groups.GroupRepository;
import com.esports.championship.emulator.groups.JdbcGroupRepository;
import com.esports.championship.emulator.groups.Round;
import com.esports.championship.emulator.teams.JdbcTeamRepository;
import com.esports.championship.emulator.teams.Team;
import com.esports.championship.emulator.teams.TeamRepository;
import com.esports.championship.emulator.testsupport.TestDataSourceFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

public class ResultsRepositoryTest {
    private DataSource dataSource = TestDataSourceFactory.create("championships");
    private JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
    private ChampionshipRepository championshipRepository = new JdbcChampionshipRepository(dataSource);
    private GroupRepository groupRepository = new JdbcGroupRepository(dataSource);
    private ResultsRepository resultsRepository = new JdbcResultsRepository(dataSource);
    private TeamRepository teamRepository = new JdbcTeamRepository(dataSource);

    @Before
    public void setUp() throws Exception {
        jdbcTemplate.execute("DELETE FROM result");
        jdbcTemplate.execute("DELETE FROM group_team");
        jdbcTemplate.execute("DELETE FROM team");
        jdbcTemplate.execute("DELETE FROM champ_group");
        jdbcTemplate.execute("DELETE FROM championship");
    }

    @Test
    public void testCreateResult() {
        Championship championship = championshipRepository.save(new Championship(1L, "PRO LEAGUE"));
        Group group = groupRepository.save(new Group(1L, championship.getId(), "A", Round.FIRST));
        Team team1 = teamRepository.save(new Team(1L, "Rocket Sports"));
        Team team2 = teamRepository.save(new Team(2L, "Golden Sports"));

        Result result = new Result(1L, team1.getId(), team1.getId(), team2.getId(),
                group.getId(), 16, 12);

        Result savedResult = resultsRepository.save(result);

        Assert.assertNotNull(savedResult);
        Assert.assertNotNull(savedResult.getId());
        Assert.assertEquals(result.getTeam1(), savedResult.getTeam1());
        Assert.assertEquals(result.getTeam2(), savedResult.getTeam2());
        Assert.assertEquals(result.getWinTeam(), savedResult.getWinTeam());
        Assert.assertEquals(result.getScore1(), savedResult.getScore1());
        Assert.assertEquals(result.getScore2(), savedResult.getScore2());
        Assert.assertEquals(result.getGroupId(), savedResult.getGroupId());

    }
}
