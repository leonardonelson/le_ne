package com.esports.championship.emulator.persistencesupport;

public interface BaseRepository<T> {
    T save(T entity);
    T find(long id);
}
