package com.esports.championship.emulator.championships;

import com.esports.championship.emulator.persistencesupport.BaseRepository;

public interface ChampionshipRepository extends BaseRepository<Championship> {
}
