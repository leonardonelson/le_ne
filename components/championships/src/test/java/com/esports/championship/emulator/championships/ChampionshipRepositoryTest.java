package com.esports.championship.emulator.championships;

import com.esports.championship.emulator.championships.Championship;
import com.esports.championship.emulator.championships.ChampionshipRepository;
import com.esports.championship.emulator.championships.JdbcChampionshipRepository;
import com.esports.championship.emulator.testsupport.TestDataSourceFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

public class ChampionshipRepositoryTest {

    private DataSource dataSource = TestDataSourceFactory.create("championships");
    private JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
    private ChampionshipRepository championshipRepository = new JdbcChampionshipRepository(dataSource);

    @Before
    public void setUp() throws Exception {
        jdbcTemplate.execute("DELETE FROM result");
        jdbcTemplate.execute("DELETE FROM group_team");
        jdbcTemplate.execute("DELETE FROM team");
        jdbcTemplate.execute("DELETE FROM champ_group");
        jdbcTemplate.execute("DELETE FROM championship");
    }

    @Test
    public void testCreateChampionship() {
        Championship championship = new Championship();
        championship.setName("PRO League");

        Championship savedChamp = championshipRepository.save(championship);

        Assert.assertNotNull(savedChamp);
        Assert.assertNotNull(savedChamp.getId());
        Assert.assertEquals(championship.getName(), savedChamp.getName());
    }
}
