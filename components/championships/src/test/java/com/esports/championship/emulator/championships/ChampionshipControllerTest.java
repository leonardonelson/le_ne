package com.esports.championship.emulator.championships;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import static org.mockito.Mockito.*;

public class ChampionshipControllerTest {

    ChampionshipRepository championshipRepository = mock(ChampionshipRepository.class);
    ChampionshipController championshipController = new ChampionshipController(championshipRepository);


    @Test
    public void testCreateChampionship() {
        // Given
        final String champName = "Liga Amadora";
        Championship championship = new Championship();
        championship.setName(champName);
        Championship mockChampionship = new Championship(1L, champName);
        doReturn(mockChampionship).when(championshipRepository).save(championship);
        // When
        ResponseEntity<Championship> response = championshipController.createChampionship(championship);
        // Then
        verify(championshipRepository).save(championship);
        Championship championshipResponse = response.getBody();
        Assert.assertEquals(HttpStatus.OK, response.getStatusCode());
        Assert.assertNotNull(championshipResponse);
        Assert.assertNotNull(championshipResponse.getId());
        Assert.assertEquals(mockChampionship.getName(), championshipResponse.getName());
        Assert.assertEquals(mockChampionship.getId(), championshipResponse.getId());
    }

}
