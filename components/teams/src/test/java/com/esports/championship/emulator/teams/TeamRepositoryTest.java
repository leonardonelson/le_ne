package com.esports.championship.emulator.teams;

import com.esports.championship.emulator.testsupport.TestDataSourceFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;

public class TeamRepositoryTest {
    private DataSource dataSource = TestDataSourceFactory.create("championships");
    private JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
    private TeamRepository teamRepository = new JdbcTeamRepository(dataSource);

    @Before
    public void setUp() throws Exception {
        jdbcTemplate.execute("DELETE FROM result");
        jdbcTemplate.execute("DELETE FROM group_team");
        jdbcTemplate.execute("DELETE FROM team");
        jdbcTemplate.execute("DELETE FROM champ_group");
        jdbcTemplate.execute("DELETE FROM championship");
    }

    @Test
    public void testCreateTeam() {
        Team team = new Team();
        team.setName("Rocket Sports");

        Team savedTeam = teamRepository.save(team);

        Assert.assertNotNull(savedTeam);
        Assert.assertNotNull(savedTeam.getId());
        Assert.assertEquals(team.getName(), savedTeam.getName());
    }
}
