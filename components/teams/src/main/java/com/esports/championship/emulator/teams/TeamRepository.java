package com.esports.championship.emulator.teams;

import com.esports.championship.emulator.persistencesupport.BaseRepository;

public interface TeamRepository extends BaseRepository<Team> {
}
