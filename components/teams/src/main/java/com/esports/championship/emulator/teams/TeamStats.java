package com.esports.championship.emulator.teams;

public class TeamStats {
    private long teamId;
    private String teamName;
    private long wins;
    private long points;
    private long balance_rounds;

    public TeamStats(long teamId, String teamName, long wins, long points, long balance_rounds) {
        this.teamId = teamId;
        this.teamName = teamName;
        this.wins = wins;
        this.points = points;
        this.balance_rounds = balance_rounds;
    }

    public TeamStats() {
    }

    public long getTeamId() {
        return teamId;
    }

    public void setTeamId(long teamId) {
        this.teamId = teamId;
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public long getWins() {
        return wins;
    }

    public void setWins(long wins) {
        this.wins = wins;
    }

    public long getPoints() {
        return points;
    }

    public void setPoints(long points) {
        this.points = points;
    }

    public long getBalance_rounds() {
        return balance_rounds;
    }

    public void setBalance_rounds(long balance_rounds) {
        this.balance_rounds = balance_rounds;
    }
}
